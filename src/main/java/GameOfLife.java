import game_of_life.entity.Cell;
import game_of_life.entity.Desk;
import game_of_life.printer.Printer;
import game_of_life.printer.PrinterImpl;
import game_of_life.rader.Reader;
import game_of_life.rader.ReaderImpl;
import game_of_life.writer.Writer;
import game_of_life.writer.WriterImpl;
import java.util.List;

public class GameOfLife {
    private static final String ALIVE = "*";
    private static final String DEFAULT_NAME = "randomStart";

    public static void main(String[] args) {
        String inputFile = DEFAULT_NAME;
        if (args[0] != null) {
            inputFile = args[0];
        }
        String outputFile = "default";
        if (args[1] != null) {
            outputFile = args[1];
        }
        int maxStepAmount;
        try {
            maxStepAmount = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            System.err.println("Thrid argument must be integer\n" + e);
            return;
        }
        if (inputFile.equals(DEFAULT_NAME)) {
            mainLoop(randomStart(), maxStepAmount, outputFile);
        } else {
            mainLoop(fileStart(inputFile), maxStepAmount, outputFile);
        }
    }

    private static Desk fileStart(String fileName) {
        Reader reader = new ReaderImpl();
        return new Desk(reader.read(fileName));
    }

    private static Desk randomStart() {
        return new Desk();
    }

    private static void mainLoop(Desk desk, int maxStepAmount, String outputFile) {
        Printer printer = new PrinterImpl();
        List<Cell> cells = desk.getCells();
        int aliveLast = 0;
        for (int step = 0; step < maxStepAmount; step++) {
            int aliveCurrent = 0;
            for (Cell cell : cells) {
                if (cell.getStatus().equals(ALIVE)) {
                    aliveCurrent++;
                }
            }
            System.out.println("step " + step + "|Got alive cells:" + aliveCurrent);
            printer.print(cells, desk.getX(), desk.getY());
            cells.forEach(Cell::calculateNextStatus);
            aliveLast = aliveCurrent;
            cells.forEach(Cell::update);
            clearScreen();
        }
        System.out.println("Last Step: " + maxStepAmount + "| Last alive cell count: "
                + aliveLast + "|\n");
        Writer writer = new WriterImpl();
        writer.write(outputFile, cells, desk.getX(), desk.getY());
    }

    private static void clearScreen() {
        try {
            Thread.sleep(120);
        } catch (InterruptedException e) {
           System.err.println("Can't clear screen\n" + e);
           return;
        }
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

}
