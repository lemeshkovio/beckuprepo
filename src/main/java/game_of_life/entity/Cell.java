package game_of_life.entity;

import java.util.ArrayList;
import java.util.List;

public class Cell {
    private CellStatus status = CellStatus.DEAD;
    private CellStatus nextStatus = CellStatus.DEAD;
    private final int xCord;
    private final int yCord;
    private final List<Cell> neighbors = new ArrayList<>();


    public Cell(int xCoord, int yCoord) {
        this.xCord = xCoord;
        this.yCord = yCoord;
    }

    public void calculateNextStatus() {
        int aliveNeighbors = 0;
        for (Cell cell : this.neighbors) {
            if (cell.getStatus().equals(CellStatus.ALIVE.getDescription())) {
                aliveNeighbors++;
            }
        }
        if (aliveNeighbors == 2) {
            this.nextStatus = this.status;
        }
        if (aliveNeighbors == 3) {
            this.nextStatus = CellStatus.ALIVE;
        }
        if (aliveNeighbors < 2 || aliveNeighbors > 3) {
            this.nextStatus = CellStatus.DEAD;
        }

    }

    public void update() {
        this.status = this.nextStatus;
    }

    public int getX() {
        return xCord;
    }

    public int getY() {
        return yCord;
    }

    public String getStatus() {
        return status.getDescription();
    }

    public void born() {
        this.status = CellStatus.ALIVE;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        return result;
    }

    public void addNeighbor(Cell neighbor) {
        this.neighbors.add(neighbor);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Cell other = (Cell) obj;
        return status == other.status;
    }

    @Override
    public String toString() {
        return "Cell (status = " + status + ", x=" + xCord + ", y=" + yCord
                + ")";
    }


}
