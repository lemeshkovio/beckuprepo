package game_of_life.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Desk {
    public static final int MIN_ALIVE_AMOUNT = 300;
    public static final int MAX_ALIVE_AMOUNT = 1300;
    private static final char ALIVE = '*';
    private final List<Cell> cells;
    private final int deskSizeX;
    private int deskSizeY;
    private static final int SIZE_Y = 54;
    private static final int SIZE_X = 90;

    public Desk() {
        Random rand = new Random();
        int count = rand.nextInt(MAX_ALIVE_AMOUNT - MIN_ALIVE_AMOUNT) + MIN_ALIVE_AMOUNT;
        this.deskSizeX = SIZE_X;
        this.deskSizeY = SIZE_Y;
        this.cells = new ArrayList<>();
        for (int y = 0; y < deskSizeY; y++) {
            for (int x = 0; x < deskSizeX; x++) {
                this.cells.add(new Cell(x, y));
            }
        }
        setNeighbors();
        sowRandomly(count);
    }



    public Desk(List<String> strings){

        this.cells = new ArrayList<>();
        this.deskSizeY = strings.size();
        if (this.deskSizeY > 54){
            this.deskSizeY = 54;
        }
        this.deskSizeX = strings.get(0).length();
        for (int y = 0; y < deskSizeY; y++){
            for (int x = 0; x < deskSizeX; x++ ){
                Cell cell = new Cell(x, y);
                if (strings.get(y).length() > x){
                    if (strings.get(y).charAt(x) == ALIVE){
                        cell.born();
                    }
                }
                cells.add(cell);
            }
        }
        setNeighbors();
    }

    public Cell getCell(int x, int y){
        if (x < 0){
            x = this.deskSizeX + x % this.deskSizeX;
        }
        if (y < 0){
            y = this.deskSizeY + y % this.deskSizeY;
        }
        if (x >= this.deskSizeX){
            x = x % this.deskSizeX;
        }
        if (y >= this.deskSizeY){
            y = y % this.deskSizeY;
        }
        return this.cells.get(y * this.deskSizeX + x);
    }

    private void setNeighbors() {
        for (Cell cell : this.cells) {
            for (int dx = -1; dx <= 1; dx++) {
                for (int dy = -1; dy <= 1; dy++) {
                    if (dx == 0 && dy == 0) {
                    } else {
                        cell.addNeighbor(getCell(cell.getX() + dx, cell.getY() + dy));
                    }
                }
            }
        }
    }

    private void sowRandomly(int seedCount){
        if (seedCount <= 0) {
            throw new IllegalArgumentException("seed must be > 0");
        }
        Random rand = new Random();
        while (seedCount > 0){
            int x = rand.nextInt(deskSizeX);
            int y = rand.nextInt(deskSizeY);
            if (getCell(x,y).getStatus().equals(CellStatus.DEAD.getDescription())){
                getCell(x,y).born();
                --seedCount;
            }
        }
    }

    public int getX() {
        return deskSizeX;
    }

    public int getY() {
        return deskSizeY;
    }

    public List<Cell> getCells() {
        return this.cells;
    }
}
