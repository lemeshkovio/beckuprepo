package game_of_life.entity;

public enum CellStatus {DEAD(" "), ALIVE("*");

    
    private final String description;

    /**
     * @param description
     */
    CellStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
