package game_of_life.printer;

import game_of_life.entity.Cell;

import java.util.List;

public interface Printer {

    void print(List<Cell> desk, int x, int y);
}
