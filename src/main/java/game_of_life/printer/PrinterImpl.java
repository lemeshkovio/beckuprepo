package game_of_life.printer;

import game_of_life.entity.Cell;

import java.util.List;

public class PrinterImpl implements Printer {

    public void print(List<Cell> nextStep, int sizeX, int sizeY) {

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                if (x == sizeX - 1){
                    System.out.print(nextStep.get(y * sizeX + x).getStatus());
                } else {
                    System.out.print(nextStep.get(y * sizeX + x).getStatus() + " ");
                }

            }
            System.out.println();
        }
    }

}
