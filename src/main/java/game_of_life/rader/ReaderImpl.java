package game_of_life.rader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ReaderImpl implements Reader{

    @Override
    public List<String> read(String fileName) {

        File file = new File(fileName + EXTENSION);
        List<String> fileContent = new ArrayList<>();

        try (Scanner scanner = new Scanner(file.getCanonicalFile())) {
            while(scanner.hasNextLine()) {
                fileContent.add(scanner.nextLine());
            }
        } catch (IOException e) {
            System.err.println("Can't read file\n" + e);
            return new LinkedList<>();
        }
        return fileContent;
    }
}
