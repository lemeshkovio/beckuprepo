package game_of_life.rader;

import java.util.List;

public interface Reader {
    static final String EXTENSION = ".txt";

    List<String> read(String fileName);
}
