package game_of_life.writer;

import game_of_life.entity.Cell;

import java.util.List;

public interface Writer {
    static final String EXTENTION = ".txt";

    void write(String fileName, List<Cell> cells, int sizeX, int sizeY);
}
