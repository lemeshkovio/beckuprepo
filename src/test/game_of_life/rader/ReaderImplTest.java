package game_of_life.rader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class ReaderImplTest {
    private static final String TEST_PREFIX = "./src/test/resources/";
    public static final String TEST_FILE = "testField";
    private ReaderImpl reader;
    @BeforeEach
    void setup(){
        reader = new ReaderImpl();
    }
    @Test
    void test_read_ShouldReturnListOfStrings() {
        List<String> expect = new ArrayList<>();
        expect.add("00000");
        expect.add("00*00");
        expect.add("00*00");
        expect.add("00*00");
        expect.add("00000");
        reader.setPREFIX(TEST_PREFIX);
        List<String> testList = reader.read(TEST_FILE);
        Assertions.assertEquals(expect,testList);
    }
}